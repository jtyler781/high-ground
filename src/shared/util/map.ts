export const getWithDefaultSet = <K, V>(map: Map<K, V>, key: K, defaultValue: () => V): V => {
	const existingValue = map.get(key);
	if (existingValue !== undefined) {
		return existingValue;
	} else {
		const newValue = defaultValue();
		map.set(key, newValue);
		return newValue;
	}
};
