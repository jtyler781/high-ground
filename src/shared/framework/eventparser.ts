import {RoomLeaveRequest} from '@shared/communication/roomleave/roomleaverequest';
import {RoomJoinRequest} from '@shared/communication/roomjoin/roomjoinrequest';
import {Message} from './message';
import {isObject, isString} from './typeguards';

export function parseMessage(dataString: string): Message | undefined {
	const data: unknown = JSON.parse(dataString);
	if (!isObject(data)) {
		console.error('Received message that is not an object. Type is ' + typeof data);
		return undefined;
	}
	const messageId = data['id'];
	if (!messageId) {
		console.error('Event was sent without an id');
		return undefined;
	} else if (!isString(messageId)) {
		console.error('Received message whose id is not a string. Type is ' + typeof messageId);
		return undefined;
	}
	switch (messageId) {
		case RoomLeaveRequest.id:
			return new RoomLeaveRequest();
		case RoomJoinRequest.id:
			if (!RoomJoinRequest.isSerializedRoomJoinRequest(data)) {
				console.error('Message with RoomJoinRequest id has incorrect property types');
				return undefined;
			}
			return RoomJoinRequest.deserialize(data);
	}

	return undefined;
}
