export class Hub<T> {
	private readonly listeners: Set<(event: T) => void> = new Set();

	public listen(onEvent: (event: T) => void): void {
		this.listeners.add(onEvent);
	}

	public notify(event: T): void {
		for (const onEvent of this.listeners) {
			onEvent(event);
		}
	}
}
