type Falsy = false | 0 | '' | null | undefined;
export const isTruthy = <T>(value: T | Falsy): value is T => !!value;

export const isString = (value: unknown): value is string => {
	return typeof value == 'string';
};

export const isNumber = (value: unknown): value is number => {
	return typeof value == 'number';
};

export const isObject = (value: unknown): value is Record<string, unknown> => {
	return typeof value == 'object';
};

export const isOpt = <T>(
	subTypeGuard: (valueInArray: unknown) => valueInArray is T,
): ((subValue: unknown) => subValue is T | undefined | null) => {
	return (subValue: unknown): subValue is T | undefined | null =>
		subValue === undefined || subValue === null || subTypeGuard(subValue);
};

export const isTypedArray = <T>(
	value: unknown,
	subTypeGuard: (valueInArray: unknown) => valueInArray is T,
): value is readonly T[] => {
	return Array.isArray(value) && value.every((valueInArray) => subTypeGuard(valueInArray));
};
