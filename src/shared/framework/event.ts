export type EventId = string;

export abstract class Event {
	public abstract readonly id: EventId;
}
