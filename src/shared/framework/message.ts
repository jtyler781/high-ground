import {isObject, isString} from './typeguards';

export type MessageId = string;

/**
 * Represents an object to send over a client-server communication in either direction.
 */
export abstract class Message {
	constructor(public readonly id: MessageId) {}
}

// TODO rename this since it's already a javascript structure
export abstract class Request extends Message {
	constructor(id: MessageId) {
		super(id);
	}

	public abstract serialize(): SerializedMessage;
}

export abstract class Push extends Message {
	constructor(id: MessageId, public readonly recipientId: string) {
		super(id);
	}

	public abstract serialize(): SerializedMessage;
}

export interface SerializedMessage {
	readonly 'id': MessageId;
}

export const isSerializedMessage = (value: unknown): value is SerializedMessage =>
	isObject(value) && isString(value['id']);
