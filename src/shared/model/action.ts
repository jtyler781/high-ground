import {Push, Request} from '@shared/framework/message';

export type Info = Record<string, any>;
export type Decision = Record<string, any>;

export interface BaseAction<T> {
	readonly playerId: string;
	readonly type: T;
}

// TODO determine when `recipientId` *and* `playerId` are needed
export abstract class ActionNotificationPush<T> extends Push implements BaseAction<T> {
	constructor(id: string, recipientId: string, public readonly type: T, public readonly playerId: string) {
		super(id, recipientId);
	}
}

export abstract class ActionPush<T> extends ActionNotificationPush<T> {
	constructor(id: string, recipientId: string, type: T, playerId: string, public readonly info?: Info) {
		super(id, recipientId, type, playerId);
	}
}

export abstract class PerformActionRequest<T> extends Request implements BaseAction<T> {
	constructor(
		id: string,
		public readonly playerId: string,
		public readonly type: T,
		public readonly decision?: Decision,
	) {
		super(id);
	}
}

export abstract class NotifyDecisionPush<T> extends Push implements BaseAction<T> {
	constructor(
		id: string,
		recipientId: string,
		public readonly type: T,
		public readonly playerId: string,
		public readonly decision?: Decision,
	) {
		super(id, recipientId);
	}
}
