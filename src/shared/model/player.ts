import {isObject, isString} from '@shared/framework/typeguards';
import {WriteablePache} from '@shared/pache/pache';

export class Player {
	public readonly name: WriteablePache<string>;

	constructor(public readonly id: string, initialName: string) {
		this.name = new WriteablePache(() => initialName);
	}

	public serialize(): SerializedPlayer {
		return {'i': this.id, 'n': this.name.getValue()};
	}

	public static createOtherPlayerInRoomFromSerializedPlayer(data: SerializedPlayer): Player {
		return new Player(data.i, data.n);
	}

	public static isSerializedPlayer = (value: unknown): value is SerializedPlayer => {
		return isObject(value) && isString(value['i']) && isString(value['n']);
	};
}

export interface SerializedPlayer {
	readonly 'i': string;
	readonly 'n': string;
}
