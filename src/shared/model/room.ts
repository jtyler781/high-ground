import {CrosswordPuzzle} from '@client/games/crossword/model/crosswordpuzzle';
import {Player} from '@shared/model/player';
import {WriteablePache} from '@shared/pache/pache';
import {PacheMap} from '@shared/pache/pachemap';

export class Room {
	public readonly players = new PacheMap<string, Player>();
	public readonly game = new WriteablePache<CrosswordPuzzle | undefined>(() => undefined);

	constructor(public readonly code: string) {}
}
