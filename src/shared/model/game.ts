import {ActionPush, BaseAction, PerformActionRequest} from './action';

export abstract class Game<T> {
	public abstract readonly minPlayers: number;
	public abstract readonly maxPlayers: number;
	public actions: Map<string, ActionPush<T>[]> = new Map();
	public decisions: PerformActionRequest<T>[] = [];

	public constructor() {}

	public abstract startGame(startingPlayerId: string): ActionPush<T>[];
	public abstract performAction(performActionRequest: PerformActionRequest<T>): BaseAction<T>[];
}
