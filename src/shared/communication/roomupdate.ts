import {isObject, isString} from '@shared/framework/typeguards';
import {Player, SerializedPlayer} from '@shared/model/player';
import {SerializedUpdate, Update, UpdatePush} from './update';

export class RoomUpdate extends Update {
	public static readonly id = 'RoomUpdate';

	constructor(playerId: string, public readonly roomChanges: SerializedRoomUpdateData) {
		super(RoomUpdate.id, playerId, roomChanges);
	}

	public static isSerializedRoomUpdateData(value: unknown): value is SerializedRoomUpdateData {
		return (
			isObject(value) &&
			isString(value['c']) &&
			(value['a'] == SerializedRoomUpdateAction.Join ||
				value['a'] == SerializedRoomUpdateAction.Leave ||
				value['a'] == SerializedRoomUpdateAction.ChangeName) && // TODO create enum typeguard
			Player.isSerializedPlayer(value['p'])
		);
	}
}

export class RoomUpdatePush extends UpdatePush {
	constructor(recipientId: string, update: SerializedUpdate) {
		super(recipientId, update);
	}
}

export interface SerializedRoomUpdateData {
	readonly 'a': SerializedRoomUpdateAction;
	readonly 'p': SerializedPlayer; // player who is doing something relative to the room
	readonly 'c': string; // Room code -- This never changes, right? Is it even needed?
}

export enum SerializedRoomUpdateAction {
	Join = 'Join',
	Leave = 'Leave',
	ChangeName = 'ChangeName',
}
