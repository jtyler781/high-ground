import {Push, SerializedMessage} from '@shared/framework/message';
import {isObject, isString, isTypedArray} from '@shared/framework/typeguards';
import {Player, SerializedPlayer} from '@shared/model/player';
import {Room} from '@shared/model/room';
import {SerializedUpdate} from '../update';

export class RoomJoinResponse extends Push {
	public static id = 'RoomJoinResponse';

	constructor(recipientId: string, private readonly room: Room, private readonly updates: SerializedUpdate[] = []) {
		super(RoomJoinResponse.id, recipientId);
	}

	public serialize(): SerializedRoomJoinResponse {
		return {
			'id': RoomJoinResponse.id,
			'c': this.room.code,
			'p': this.room.players.values().map((player) => player.serialize()),
			'u': this.updates,
		};
	}

	public static deserialize(data: SerializedRoomJoinResponse): {
		room: Room;
		serializedUpdates: readonly SerializedUpdate[];
	} {
		const room = new Room(data['c']);
		for (const serializedPlayer of data['p']) {
			const player = Player.createOtherPlayerInRoomFromSerializedPlayer(serializedPlayer);
			room.players.set(player.id, player);
		}
		const serializedUpdates = data['u'] ?? [];
		return {room, serializedUpdates};
	}

	public static isSerializedRoomJoinResponse(value: unknown): value is SerializedRoomJoinResponse {
		return isObject(value) && isString(value['c']) && isTypedArray(value['p'], Player.isSerializedPlayer);
	}
}

export interface SerializedRoomJoinResponse extends SerializedMessage {
	readonly 'c': string; // Room code -- This never changes, right? Is it even needed?
	readonly 'p': readonly SerializedPlayer[]; // Players already in the room
	readonly 'u': SerializedUpdate[];
}
