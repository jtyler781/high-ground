import {MessageId} from '@shared/framework/message';
import {isObject, isString} from '@shared/framework/typeguards';
import {Player, SerializedPlayer} from '@shared/model/player';
import {Request} from '@shared/framework/message';

export class RoomJoinRequest extends Request {
	public static readonly id = 'RoomJoinRequest';

	constructor(public readonly player: Player, public readonly roomCode: string) {
		super(RoomJoinRequest.id);
	}

	public serialize(): SerializedRoomJoinRequest {
		return {
			'id': RoomJoinRequest.id,
			'p': this.player.serialize(),
			'r': this.roomCode,
		};
	}

	public static deserialize(data: SerializedRoomJoinRequest): RoomJoinRequest {
		return new RoomJoinRequest(Player.createOtherPlayerInRoomFromSerializedPlayer(data['p']), data['r']);
	}

	public static isSerializedRoomJoinRequest = (value: unknown): value is SerializedRoomJoinRequest => {
		return (
			isObject(value) && isString(value['id']) && Player.isSerializedPlayer(value['p']) && isString(value['r'])
		);
	};
}

export interface SerializedRoomJoinRequest {
	readonly 'id': MessageId;
	readonly 'p': SerializedPlayer;
	readonly 'r': string;
}
