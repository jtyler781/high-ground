import {MessageId, SerializedMessage} from '@shared/framework/message';
import {Request} from '@shared/framework/message';
import {isObject, isString} from '@shared/framework/typeguards';

export class Update<T = unknown> extends Request {
	constructor(public readonly id: MessageId, public readonly playerId: string, public readonly data: T) {
		super(id);
	}

	public serialize(): SerializedUpdate {
		return {
			'id': this.id,
			'p': this.playerId,
			'd': this.data,
		};
	}

	public static deserialize(data: SerializedUpdate): Update {
		return new Update(data['id'], data['p'], data['d']);
	}

	public static isSerializedUpdate(value: unknown): value is SerializedUpdate {
		return isObject(value) && isString(value['id']) && isString(value['p']) && 'd' in value;
	}
}

export class UpdatePush<T = unknown> extends Update<T> {
	constructor(public readonly recipientId: string, update: SerializedUpdate<T>) {
		super(update['id'], update['p'], update['d']);
	}
}

export interface SerializedUpdate<T = unknown> extends SerializedMessage {
	readonly 'p': string; // Player id
	readonly 'd': T; // data
}
