import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class RestCaller {
	// In order to use `HttpClient`, we have to import `HttpClientModule` somewhere --> see root module of app
	constructor(private readonly http: HttpClient) {}

	public async get<T>(url: string): Promise<T> {
		const response: T = await this.http.get<T>(url, {responseType: 'json'}).toPromise();
		return response;
	}
}
