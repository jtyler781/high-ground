import {Request, SerializedMessage} from '@shared/framework/message';

export class RoomLeaveRequest extends Request {
	public static readonly id = 'RoomLeaveRequest';

	constructor() {
		super(RoomLeaveRequest.id);
	}

	public serialize(): SerializedRoomLeaveRequest {
		return {'id': RoomLeaveRequest.id};
	}
}

export interface SerializedRoomLeaveRequest extends SerializedMessage {}
