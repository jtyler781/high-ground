// import {SerializedMessage} from '@shared/framework/message';
// import {ActionNotificationPush, Info, ActionPush} from '@shared/model/action';

// export enum RookActionType {
// 	GetHand = 'h',
// 	BidOrPass = 'b',
// 	NestToWidow = 'n',
// 	LeadTrick = 'l',
// 	FollowTrick = 'f',
// }

// // TODO map action type to Info and Decision types

// export class RookActionNotificationPush extends ActionNotificationPush<RookActionType> {
// 	public static readonly id: string = 'RookActionNotificationPush';

// 	constructor(
// 		recipientId: string,
// 		type: RookActionType,
// 		playerId: string,
// 		id: string = RookActionNotificationPush.id,
// 	) {
// 		super(id, recipientId, type, playerId);
// 	}

// 	public serialize(): SerializedRookActionNotificationPush {
// 		return {
// 			'id': this.id,
// 			'p': this.playerId,
// 			't': this.type,
// 		};
// 	}

// 	public static deserialize(data: SerializedRookActionNotificationPush): RookActionNotificationPush {
// 		return new RookActionNotificationPush('', data['t'], data['p'], data['id']);
// 	}
// }

// export class RookActionPush extends ActionPush<RookActionType> {
// 	public static readonly id: string = 'RookActionPush';

// 	constructor(recipientId: string, type: RookActionType, playerId: string, info?: Info) {
// 		super(RookActionNotificationPush.id, recipientId, type, playerId, info);
// 	}

// 	public serialize(): SerializedRookActionPush {
// 		return {
// 			'id': this.id,
// 			'p': this.playerId,
// 			't': this.type,
// 			'i': {}, // TODO info
// 		};
// 	}

// 	public static deserialize(data: SerializedRookActionPush): RookActionPush {
// 		return new RookActionPush('', data['t'], data['p'], data['i']);
// 	}
// }

// export interface SerializedRookActionNotificationPush extends SerializedMessage {
// 	't': RookActionType;
// 	'p': string;
// }

// export interface SerializedRookActionPush extends SerializedRookActionNotificationPush {
// 	'i': Info; // TODO
// }
