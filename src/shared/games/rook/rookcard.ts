// import {Injectable} from '@angular/core';

// export enum RookColor {
// 	Red = 'r',
// 	Green = 'g',
// 	Yellow = 'y',
// 	Black = 'b',
// }

// export enum RookPoints {
// 	Zero = 0,
// 	Five = 5,
// 	Ten = 10,
// 	Twenty = 20,
// }

// /** 0 is Rook */
// export type RookCardValue = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14;

// export class RookCard {
// 	constructor(
// 		public suit: RookColor, // Rook color is editable
// 		public readonly value: RookCardValue,
// 		public readonly points: RookPoints,
// 	) {}
// }

// // TODO does this need to be injectable if there's so many static properties?
// @Injectable()
// export class RookDeck {
// 	public static valuesToPoints: Record<RookCardValue, RookPoints> = {
// 		0: RookPoints.Twenty,
// 		1: RookPoints.Zero,
// 		2: RookPoints.Zero,
// 		3: RookPoints.Zero,
// 		4: RookPoints.Zero,
// 		5: RookPoints.Five,
// 		6: RookPoints.Zero,
// 		7: RookPoints.Zero,
// 		8: RookPoints.Zero,
// 		9: RookPoints.Zero,
// 		10: RookPoints.Ten,
// 		11: RookPoints.Zero,
// 		12: RookPoints.Zero,
// 		13: RookPoints.Zero,
// 		14: RookPoints.Ten,
// 	};
// 	public readonly cards: RookCard[] = RookDeck.generateCards();

// 	/** Generates the 57 cards in a Rook deck */
// 	private static generateCards(): RookCard[] {
// 		const cards: RookCard[] = [];
// 		const colors: RookColor[] = [RookColor.Red, RookColor.Green, RookColor.Yellow, RookColor.Black];
// 		const values: RookCardValue[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
// 		cards.push(new RookCard(RookColor.Red, 0, this.valuesToPoints[0])); // the Rook card
// 		for (const color of colors) {
// 			for (const value of values) {
// 				cards.push(new RookCard(color, value, this.valuesToPoints[value]));
// 			}
// 		}
// 		console.log(`Rook deck generated with ${cards.length} cards`);
// 		return cards;
// 	}

// 	/** Shuffles an array in place */
// 	public static shuffle(cards: RookCard[]): void {
// 		for (let i = cards.length - 1; i > 0; i--) {
// 			const j = Math.floor(Math.random() * (i + 1));
// 			[cards[i], cards[j]] = [cards[j], cards[i]];
// 		}
// 	}
// }
