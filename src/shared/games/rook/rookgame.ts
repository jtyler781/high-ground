// import {Injectable} from '@angular/core';
// import {ActionPush, BaseAction, PerformActionRequest} from '@shared/model/action';
// import {Game} from '@shared/model/game';
// import {Player} from '@shared/model/player';
// import {RookActionNotificationPush, RookActionPush, RookActionType} from './rookaction';
// import {RookCard, RookColor, RookDeck} from './rookcard';

// export enum RookTeam {
// 	Bidding = 'b',
// 	Opposing = 'o',
// }

// export class RookTrick {
// 	public suit: RookColor;
// 	public starter: string;
// 	public winningTeam: RookTeam;
// 	public cards: Map<string, RookCard>;
// }

// /**
//  * TODO There will be different copies of the game, with different levels of info.
//  * The omniscient version of the game includes all details that each player knows and the information not yet revealed.
//  * The public version of the game includes the visible information that someone who just walked into the room can see.
//  * The private versions of the game are copies of the public version plus details that the version's player knows.
//  */
// export class RookGame extends Game<RookActionType> {
// 	public readonly minPlayers: number = 4;
// 	public readonly maxPlayers: number = 4;
// 	public readonly handSize = 13;

// 	public teams: Map<RookTeam, string[]>;
// 	public hands: Map<string, RookCard[]>;
// 	public trump: RookColor;
// 	public tricks: RookTrick[];
// 	public nest: RookCard[];

// 	constructor(public readonly players: Player[], private readonly rookDeck: RookDeck) {
// 		super();
// 	}

// 	public startGame(startingPlayerId: string): ActionPush<RookActionType>[] {
// 		// TODO when is the right time to set the teams? We don't know yet the bidding/opposing state
// 		this.teams = new Map<RookTeam, string[]>([
// 			[RookTeam.Bidding, [this.players[0].id, this.players[2].id]],
// 			[RookTeam.Opposing, [this.players[1].id, this.players[3].id]],
// 		]);
// 		const deck = this.rookDeck.cards;
// 		RookDeck.shuffle(this.rookDeck.cards);
// 		for (const [index, playerId] of this.players.map((p) => p.id).entries()) {
// 			this.hands.set(playerId, deck.slice(this.handSize * index, this.handSize * (index + 1)));
// 		}
// 		this.nest = deck.slice(this.handSize * this.players.length);
// 		const deliverHandsPushes = this.players.map(
// 			(p) => new RookActionPush(p.id, RookActionType.GetHand, p.id, {'hand': this.hands.get(p.id)}),
// 		);
// 		// TODO should we wait for players to ready up after getting their hands, or allow starting player to bid as soon as they get their hand?
// 		// TODO how do we tell players what decisions other players made?
// 		const startingPlayerPushes = this.players.map((p) =>
// 			p.id == startingPlayerId
// 				? new RookActionPush(p.id, RookActionType.BidOrPass, p.id)
// 				: new RookActionNotificationPush(p.id, RookActionType.BidOrPass, startingPlayerId),
// 		);
// 		return [...deliverHandsPushes, ...startingPlayerPushes];
// 	}

// 	public performAction(performActionRequest: PerformActionRequest<RookActionType>): BaseAction<RookActionType>[] {
// 		// Check if that what the user is trying to do actually follows all the rules
// 		const decision = performActionRequest.decision;
// 		if (decision) {
// 		} else {
// 			// TODO double check if this action type needed a decision or just acknowledgement
// 		}
// 		return [];
// 	}
// }

// @Injectable()
// export class RookGameFactory {
// 	constructor(private readonly rookDeck: RookDeck) {}

// 	public create(players: Player[]): RookGame {
// 		return new RookGame(players, this.rookDeck);
// 	}
// }
