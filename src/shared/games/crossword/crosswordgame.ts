import {ActionPush, BaseAction, PerformActionRequest} from '@shared/model/action';
import {Game} from '@shared/model/game';
import {CrosswordActionType} from './actions/crosswordactiontype';

export class CrosswordGame extends Game<CrosswordActionType> {
	public readonly minPlayers: number = 1;
	public readonly maxPlayers: number = 10;
	public readonly actions: Map<string, ActionPush<CrosswordActionType>[]> = new Map();

	public constructor() {
		super();
	}

	public startGame(startingPlayerId: string): ActionPush<CrosswordActionType>[] {
		return [];
	}

	public performAction(
		performActionRequest: PerformActionRequest<CrosswordActionType>,
	): BaseAction<CrosswordActionType>[] {
		return [];
	}
}
