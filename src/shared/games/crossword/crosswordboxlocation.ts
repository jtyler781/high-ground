export class CrosswordBoxLocation {
	constructor(readonly row: number, readonly column: number) {}

	public serialize(): string {
		return `${this.row}-${this.column}`;
	}

	public static deserialize(location: string): CrosswordBoxLocation {
		const locationMatch = location.match(/\b[\d]+\b/g);
		const row = Number(locationMatch?.[0]);
		const column = Number(locationMatch?.[1]);
		return new CrosswordBoxLocation(row, column);
	}

	public equals(that: CrosswordBoxLocation): boolean {
		return this.row == that.row && this.column == that.column;
	}

	public static equals = (a: CrosswordBoxLocation, b: CrosswordBoxLocation) => a.equals(b);
}
