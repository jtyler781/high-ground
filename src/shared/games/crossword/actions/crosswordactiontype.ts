export enum CrosswordActionType {
	SetLetter = 'l',
	SelectSpace = 's',
	SetDirection = 'd',
}
