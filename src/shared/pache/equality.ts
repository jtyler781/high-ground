export const strictEquals = (a: unknown, b: unknown): boolean => a === b;

export const arrayEquals = <T>(
	arrayA: readonly T[],
	arrayB: readonly T[],
	subEquals: (a: T, b: T) => boolean = strictEquals,
): boolean => arrayA.length === arrayB.length && arrayA.every((a, index) => subEquals(a, arrayB[index]));

export const optEquals =
	<T>(equals: (a: T, b: T) => boolean = strictEquals) =>
	(a: T | undefined, b: T | undefined) =>
		(a !== undefined && b !== undefined && equals(a, b)) || (a === undefined && b === undefined);
