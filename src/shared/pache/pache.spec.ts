import {ReadonlyPache, WriteablePache} from './pache';
import 'jasmine';

describe('pache', () => {
	const setup = () => {
		const namePache = new WriteablePache<string>(() => 'Jake Tyler');
		const firstNamePache = new ReadonlyPache<string>(() => namePache.getValue().split(' ')[0]);
		return {namePache, firstNamePache};
	};

	it('should provide updated value when direct dependency is dirty', () => {
		const {namePache, firstNamePache} = setup();
		expect(namePache.getValue()).toBe('Jake Tyler', 'pre-condition: should have expected name');
		expect(firstNamePache.getValue()).toBe('Jake', 'should provide initial value');

		namePache.setValue('dolly parton');
		expect(firstNamePache.getValue()).toBe(
			'dolly',
			'should provide updated value after setting new value on dependency',
		);
	});

	it('should not recalculate when transitive dependency updates but direct dependency resolves to same value', () => {
		const {namePache, firstNamePache} = setup();
		let recalculatedLoudFirstNamePache: number = 0;
		const loudFirstNamePache = new ReadonlyPache<string>(() => {
			recalculatedLoudFirstNamePache++;
			return firstNamePache.getValue().toUpperCase();
		});
		expect(namePache.getValue()).toBe('Jake Tyler');
		expect(loudFirstNamePache.getValue()).toBe('JAKE');
		expect(recalculatedLoudFirstNamePache).toBe(1);

		namePache.setValue('Jake Tyler');
		expect(loudFirstNamePache.dirty).toBe(
			false,
			'should not become dirty when writeable dependency was changed to same value',
		);
		expect(loudFirstNamePache.findDirtyTransitiveDependencies().length).toBe(
			0,
			'should not have any dirty dependencies when writeable dependency was changed to same value',
		);
		const unused = loudFirstNamePache.getValue(); // Simulate needing readonly pache value again
		expect(recalculatedLoudFirstNamePache).toBe(
			1,
			'should not have recalculated when none of its dependencies or itself are dirty',
		);

		namePache.setValue('Jake Ryan');
		expect(loudFirstNamePache.getValue()).toBe('JAKE', 'should not update pache value');
		expect(recalculatedLoudFirstNamePache).toBe(
			1,
			'should not recalculate readonly pache when none of its direct dependencies change',
		);
	});

	it('should update when a pache it depends on updates its value', () => {
		const {namePache, firstNamePache} = setup();
		let recalculatedLoudFirstNamePache: number = 0;
		const loudFirstNamePache = new ReadonlyPache<string>(() => {
			recalculatedLoudFirstNamePache++;
			return firstNamePache.getValue().toUpperCase();
		});
		expect(loudFirstNamePache.getValue()).toBe('JAKE');
		expect(recalculatedLoudFirstNamePache).toBe(1, 'should have calculated only once for first getValue');

		namePache.setValue('Betty White');
		expect(loudFirstNamePache.findDirtyTransitiveDependencies().length).toBeGreaterThan(
			0,
			'should have at least one dirty transitive dependency after changing value of writeable pache dependency, meaning that the readonly pache might need to recalculate',
		);
		expect(recalculatedLoudFirstNamePache).toBe(1, 'should not recalculate before calling getValue again');

		expect(loudFirstNamePache.getValue()).toBe('BETTY');
		expect(recalculatedLoudFirstNamePache)
			.withContext('should recalculate pache when one of its direct dependencies have updated')
			.toBe(2);
	});

	it('should short circuit and not depend on paches that it did not need during latest recalculation', () => {
		const {firstNamePache} = setup();
		const locationPache = new WriteablePache<string>(() => 'Utah, USA');
		const statePache = new ReadonlyPache<string>(() => locationPache.getValue().split(', ')[0]);
		const favoriteColorPache = new WriteablePache<string | undefined>(() => 'yellow');
		let recalculateFriendInformationPache: number = 0;
		const friendInformationPache = new ReadonlyPache<string>(() => {
			recalculateFriendInformationPache++;
			return firstNamePache.getValue() + ' ' + (favoriteColorPache.getValue() ?? statePache.getValue());
		});

		// Get first value
		expect(friendInformationPache.getValue()).toBe(
			'Jake yellow',
			'should have value that does not depend on pache behind short-circuit',
		);
		expect(recalculateFriendInformationPache).toBe(1, 'should have calculated pache only once for first getValue');
		expect(friendInformationPache.dependencies.size).toBe(
			2,
			'should short-circuit and only depend on the first two paches',
		);

		// Dirty pache that we could have depended on but instead short-circuited
		locationPache.setValue('Virginia, USA');
		expect(statePache.dirty).toBe(true, 'should dirty the state pache');
		expect(friendInformationPache.findDirtyTransitiveDependencies().length).toBe(
			0,
			'should not have any dirty dependencies since the dirty pache was behind short-circuit',
		);
		expect(friendInformationPache.getValue()).toBe(
			'Jake yellow',
			'should have same value after changing pache independency',
		);
		expect(statePache.dirty).toBe(
			true,
			'should not have resolved dirty pache that was behind short-circuit after another getValue',
		);
		expect(recalculateFriendInformationPache).toBe(
			1,
			'should not have recalculated when pache independency changes',
		);

		// Introduce dependency on third pache
		favoriteColorPache.setValue(undefined);
		expect(friendInformationPache.dirty).toBe(true, 'pache should be dirty when a direct dependency changes');
		expect(statePache.dirty).toBe(true, 'state pache should still be dirty after');
		expect(friendInformationPache.findDirtyTransitiveDependencies().length).toBe(
			0,
			'should not have any dirty dependencies still since the pache should not depend on the only other dirty pache',
		);
		expect(friendInformationPache.dependencies.size).toBe(
			2,
			'should still not depend on pache behind short-circuit before calling getValue again',
		);
		expect(friendInformationPache.getValue()).toBe(
			'Jake Virginia',
			'should have the completely updated pache value',
		);
		expect(recalculateFriendInformationPache).toBe(2, 'should have recalculated when pache dependency changes');
		expect(friendInformationPache.dependencies.size).toBe(
			3,
			'should finally depend on third pache after calling getValue and no longer short-circuiting',
		);
		expect(friendInformationPache.dirty).toBe(false, 'should no longer be dirty');
		expect(statePache.dirty).toBe(false, 'should have resolved the dirty pache');

		// Remove dependency on third pache
		favoriteColorPache.setValue('blue');
		expect(friendInformationPache.dependencies.size).toBe(
			3,
			'should still depend on all paches since last recalculation did not short-circuit',
		);
		expect(friendInformationPache.getValue()).toBe(
			'Jake blue',
			'should have updated pache value to trigger another short-circuit',
		);
		expect(recalculateFriendInformationPache).toBe(
			3,
			'should have recalculated when triggering short-circuit to remove dependency',
		);
		expect(friendInformationPache.dependencies.size).toBe(
			2,
			'should no longer depend on pache behind short-circuiting',
		);

		// Make sure dependency is gone
		locationPache.setValue('Vermont, USA');
		expect(friendInformationPache.findDirtyTransitiveDependencies().length).toBe(
			0,
			'should not have dirty transitive dependencies after no longer depending on pache behind short-circuiting',
		);
		expect(friendInformationPache.getValue()).toBe(
			'Jake blue',
			'should not update since it short-circuited before depending on dirty pache',
		);
		expect(recalculateFriendInformationPache).toBe(
			3,
			'should not have recalculated after pache that it no longer depends on updates',
		);
	});
});
