import {strictEquals} from './equality';
import {Pache, ReadonlyPache, WriteablePache} from './pache';

export class PacheMap<K, V> {
	private readonly map = new Map<K, WriteablePache<V>>();
	private readonly _size = new WriteablePache<number>(() => 0);
	public readonly size: Pache<number> = this._size;

	public readonly valuesPache = new ReadonlyPache<readonly V[]>(() => this._values());

	constructor(private readonly valueEquals: (a: V, b: V) => boolean = strictEquals) {}

	// *****************************
	// *** GETTERS *****************
	// *****************************

	public get(key: K): V | undefined {
		return this.map.get(key)?.getValue();
	}

	public has(key: K): boolean {
		return this.map.has(key);
	}

	public entries() {
		this._size.getValue(); // Depend on size, so any paches that depend on this entries list invalidate when an item is added or removed
		return this.map.entries();
	}

	private _values(): readonly V[] {
		return Array.from(this.entries()).map(([key, value]) => value.getValue());
	}

	public values(): readonly V[] {
		return this.valuesPache.getValue();
	}

	// *****************************
	// *** SETTERS *****************
	// *****************************

	public set(key: K, value: V): this {
		const pache = this.map.get(key);
		if (pache) {
			pache.setValue(value);
		} else {
			this.map.set(key, new WriteablePache(() => value, this.valueEquals));
		}
		this.updatePachedSize();
		return this;
	}

	public clear(): void {
		this.map.clear();
		this.updatePachedSize();
	}

	public delete(key: K): boolean {
		const result = this.map.delete(key);
		this.updatePachedSize();
		return result;
	}

	private updatePachedSize(): void {
		this._size.setValue(this.map.size);
	}
}
