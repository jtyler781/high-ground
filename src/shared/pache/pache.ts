import {strictEquals} from './equality';

const readonlyPacheRecalculationStack: PacheDependencyTreeBranchNode[] = [];

interface PacheDependencyTreeNode {
	dependents: Set<PacheDependencyTreeBranchNode>;
	findDirtyTransitiveDependencies(): PacheDependencyTreeNode[];
	needsRecalculate(): boolean;
	getValue(): unknown;
	notifyThatRunningGetValueAgainMightReturnADifferentValue(): void;
}

interface PacheDependencyTreeBranchNode extends PacheDependencyTreeNode {
	dirty: boolean;
	dependencies: Set<PacheDependencyTreeNode>;
}

interface Initialized<T> {
	value: T;
}

export abstract class Pache<T> implements PacheDependencyTreeNode {
	public readonly dependents: Set<PacheDependencyTreeBranchNode> = new Set();
	protected cache?: Initialized<T>;
	private readonly listenerCallbacks = new Set<() => void>();

	constructor(private readonly equals: (oldValue: T, newValue: T) => boolean) {}

	public abstract getValue(): T;
	public abstract findDirtyTransitiveDependencies(): PacheDependencyTreeNode[];
	public abstract needsRecalculate(): boolean;

	public listenForWhenRunningGetValueAgainMightReturnADifferentValue(callback: () => void): void {
		this.listenerCallbacks.add(callback);
	}

	public stopListeningForWhenRunningGetValueAgainMightReturnADifferentValue(callback: () => void): void {
		this.listenerCallbacks.delete(callback);
	}

	public notifyThatRunningGetValueAgainMightReturnADifferentValue(): void {
		for (const callback of this.listenerCallbacks) {
			callback();
		}
		for (const pache of this.dependents) {
			pache.notifyThatRunningGetValueAgainMightReturnADifferentValue();
		}
	}

	protected registerDependent(): void {
		if (readonlyPacheRecalculationStack.length == 0) {
			return;
		}
		const lastPache = readonlyPacheRecalculationStack[readonlyPacheRecalculationStack.length - 1];
		lastPache.dependencies.add(this);
		this.dependents.add(lastPache);
	}

	protected dirtyDependents(): void {
		for (const pache of this.dependents) {
			pache.dirty = true;
		}
	}

	protected updateCache(value: T): void {
		if (!this.cache || !this.equals(this.cache.value, value)) {
			if (!this.cache) {
				this.cache = {value};
			} else {
				this.cache.value = value;
			}
			this.dirtyDependents();
			this.notifyThatRunningGetValueAgainMightReturnADifferentValue();
		}
	}
}

export class ReadonlyPache<T> extends Pache<T> implements PacheDependencyTreeBranchNode {
	public readonly dependencies: Set<PacheDependencyTreeNode> = new Set();
	public dirty: boolean = true;

	constructor(private readonly calculate: () => T, equals: (oldValue: T, newValue: T) => boolean = strictEquals) {
		super(equals);
	}

	public findDirtyTransitiveDependencies(): PacheDependencyTreeNode[] {
		return Array.from(this.dependencies).filter((pache) => pache.needsRecalculate());
	}

	public needsRecalculate(): boolean {
		return this.dirty;
	}

	private resolveDirtyDependencies(): void {
		const dirtyDependencies = this.findDirtyTransitiveDependencies();
		for (const dirtyDependency of dirtyDependencies) {
			dirtyDependency.getValue();
		}
		if (this.findDirtyTransitiveDependencies().length > 0) {
			this.resolveDirtyDependencies();
		}
	}

	public getValue(): T {
		super.registerDependent();
		this.resolveDirtyDependencies();
		if (this.needsRecalculate()) {
			this.recalculate();
		}
		return this.cache!.value;
	}

	private recalculate(): T {
		this.resetDependencies();
		readonlyPacheRecalculationStack.push(this);
		const newValue = this.calculate();
		readonlyPacheRecalculationStack.pop();
		super.updateCache(newValue);
		this.dirty = false;
		return this.getValue();
	}

	private resetDependencies(): void {
		for (const dependency of this.dependencies) {
			dependency.dependents.delete(this);
		}
		this.dependencies.clear();
	}
}

export class WriteablePache<T> extends Pache<T> {
	constructor(private readonly initialValue: () => T, equals: (oldValue: T, newValue: T) => boolean = strictEquals) {
		super(equals);
	}

	public getValue(): T {
		if (!this.cache) {
			this.cache = {value: this.initialValue()};
		}
		this.registerDependent();
		return this.cache.value;
	}

	public setValue(value: T): void {
		super.updateCache(value);
	}

	public findDirtyTransitiveDependencies(): PacheDependencyTreeNode[] {
		return [];
	}

	public needsRecalculate(): boolean {
		return false;
	}
}
