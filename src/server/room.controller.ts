import {Room} from '@shared/model/room';
import {RoomUpdate, RoomUpdatePush, SerializedRoomUpdateAction} from '@shared/communication/roomupdate';
import {Push} from '@shared/framework/message';
import {SerializedUpdate, UpdatePush} from '@shared/communication/update';
import {getWithDefaultSet} from '@shared/util/map';
import {RoomJoinResponse} from '@shared/communication/roomjoin/roomjoinresponse';
import {RoomJoinRequest} from '@shared/communication/roomjoin/roomjoinrequest';

export class RoomController {
	private readonly rooms: Map<string, Room> = new Map();
	private readonly updatesInRooms = new Map<Room, SerializedUpdate[]>();

	public getRoom(roomCode: string): Room | undefined {
		return this.rooms.get(roomCode);
	}

	public handleUpdate(update: SerializedUpdate, roomCode: string): Push[] {
		const room = this.getRoom(roomCode);
		if (!room) {
			return [];
		}
		const updates = this.updatesInRooms.get(room) ?? [];
		updates.push(update);
		this.updatesInRooms.set(room, updates);
		return room.players
			.values()
			.filter((player) => player.id != update['p'])
			.map((player) => new UpdatePush(player.id, update));
	}

	public addPlayerToRoom(event: RoomJoinRequest): Push[] {
		const {player, roomCode} = event;
		const room: Room = getWithDefaultSet(this.rooms, roomCode, () => new Room(roomCode));
		if (room.players.has(player.id)) {
			return [];
		}
		const playersAlreadyInRoom = room.players.values();
		room.players.set(player.id, player);

		const roomUpdate = new RoomUpdate(player.id, {
			'a': SerializedRoomUpdateAction.Join,
			'p': player.serialize(),
			'c': roomCode,
		});
		const serializedRoomUpdate = roomUpdate.serialize();
		const serializedUpdatesInRoom = this.updatesInRooms.get(room);
		const roomUpdates = [
			new RoomJoinResponse(player.id, room, serializedUpdatesInRoom),
			...playersAlreadyInRoom.map((player) => new RoomUpdatePush(player.id, serializedRoomUpdate)),
		];
		return roomUpdates;
	}

	public removePlayerFromRoom(playerId: string, roomCode: string): Push[] {
		const room: Room | undefined = this.rooms.get(roomCode);
		if (room == undefined) {
			return []; // Player is trying to leave a room that does not exist
		}
		const playerLeavingRoom = room.players.get(playerId);
		if (!playerLeavingRoom) {
			return []; // Player is trying to leave a room that they are not in
		}
		room.players.delete(playerId);

		if (room.players.size.getValue() > 0) {
			this.rooms.set(roomCode, room);
		} else {
			this.rooms.delete(roomCode);
		}
		const roomUpdate = new RoomUpdate(playerId, {
			'a': SerializedRoomUpdateAction.Leave,
			'p': playerLeavingRoom.serialize(),
			'c': roomCode,
		});
		const serializedRoomUpdate = roomUpdate.serialize();
		const playersStillInRoom = room.players.values();
		return playersStillInRoom.map((player) => new RoomUpdatePush(player.id, serializedRoomUpdate));
	}
}
