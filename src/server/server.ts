import {createServer, Server} from 'http';
import express from 'express';
import * as WebSocket from 'ws';
import {RoomController} from './room.controller';
import {parseMessage} from '@shared/framework/eventparser';
import {RoomLeaveRequest} from '@shared/communication/roomleave/roomleaverequest';
import {Push} from '@shared/framework/message';
import {Update} from '@shared/communication/update';
import {RoomJoinRequest} from '@shared/communication/roomjoin/roomjoinrequest';

interface ExtWebSocket extends WebSocket {
	isAlive: boolean;
	playerId: string;
	roomCode: string | undefined;
}

export class HighGroundServer {
	public static readonly PORT: number = 8080;
	private readonly app: express.Application = express();
	private readonly server: Server = createServer(this.app);
	private readonly port: string | number = process.env.PORT || HighGroundServer.PORT;

	private readonly roomController: RoomController = new RoomController(); // Should this be an injectable?
	private readonly wss: WebSocket.Server = new WebSocket.Server({server: this.server});

	private readonly clients = new Map<string, ExtWebSocket>();

	constructor() {
		this.wss.on('connection', (ws: WebSocket) => {
			console.log('A new client has connected');

			const extWs = ws as ExtWebSocket;
			extWs.isAlive = true;
			ws.on('pong', () => (extWs.isAlive = true));

			ws.on('message', (msg: string) => {
				const event = parseMessage(msg);
				if (event instanceof RoomJoinRequest) {
					extWs.playerId = event.player.id;
					extWs.roomCode = event.roomCode;
					this.clients.set(event.player.id, extWs);
					const responses = this.roomController.addPlayerToRoom(event);
					this.pushToClients(responses);
					return;
				}
				const roomCode = extWs.roomCode;
				if (roomCode === undefined) {
					console.error('player is trying to do something with a room but not in a room');
					return;
				}
				if (event instanceof RoomLeaveRequest) {
					const roomUpdatePush = this.roomController.removePlayerFromRoom(extWs.playerId, roomCode);
					this.pushToClients(roomUpdatePush);
					extWs.roomCode = undefined;
				}
				const messageJSON: unknown = JSON.parse(msg);
				if (Update.isSerializedUpdate(messageJSON)) {
					this.pushToClients(this.roomController.handleUpdate(messageJSON, roomCode));
				}
			});

			ws.on('close', () => this.handlePlayerLeaving(ws));
			ws.on('error', (err) => {
				console.warn(`Client disconnected - reason: ${err}`);
				this.handlePlayerLeaving(ws);
			});
		});

		// Check that all connections are still connected
		const secondsBetweenChecks = 10;
		const msPerSecond = 1000;
		setInterval(() => {
			for (const ws of this.wss.clients) {
				const extWs = ws as ExtWebSocket;
				if (!extWs.isAlive) {
					return ws.terminate();
				}
				extWs.isAlive = false;
				ws.ping(null, undefined);
			}
		}, secondsBetweenChecks * msPerSecond);

		this.server.listen(this.port, () => console.log(`Server started on port ${this.port}`));
	}

	private handlePlayerLeaving(ws: WebSocket): void {
		// TODO remove player from room only when they aren't in a game
		const extWs = ws as ExtWebSocket;
		const roomCode = extWs.roomCode;
		if (roomCode) {
			const roomUpdatePush = this.roomController.removePlayerFromRoom(extWs.playerId, roomCode);
			if (roomUpdatePush != undefined) {
				this.pushToClients(roomUpdatePush);
			}
		}
	}

	public getApp(): express.Application {
		return this.app;
	}

	private pushToClients(pushes: Push[]): void {
		for (const push of pushes) {
			console.log(`Sending ${push.id} to ${push.recipientId}`);
			const client = this.clients.get(push.recipientId);
			if (client == undefined) {
				continue;
			}
			const serializedPush = JSON.stringify(push.serialize());
			try {
				client.send(serializedPush);
			} catch (e) {
				console.log({
					message: 'There was an error trying to send a push to a client',
					clientId: push.recipientId,
					error: e,
				});
			}
		}
	}
}
