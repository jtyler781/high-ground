import {ChangeDetectorRef, NgZone, OnDestroy, Pipe, PipeTransform} from '@angular/core';
import {Pache} from '@shared/pache/pache';

/*
 * Extracts the value from a pache and watches for changes to the pache. Runs change detection when the pache value
 * changes.
 *
 * Usage:
 *   myPache | pache
 * Example:
 * `<game [gameInput]="gamePache | pache"></game>`
 * The game component will get `gamePache.getValue()` passed into `gameInput`, and run change detection when the pache
 * might have changed
 */
@Pipe({name: 'pache', pure: false})
export class PachePipe implements PipeTransform, OnDestroy {
	private pache?: Pache<unknown>;
	private readonly runChangeDetection: () => void;

	constructor(changeDetectorRef: ChangeDetectorRef, ngZone: NgZone) {
		this.runChangeDetection = () => ngZone.run(() => changeDetectorRef.markForCheck());
	}

	private startListening(): void {
		this.pache?.listenForWhenRunningGetValueAgainMightReturnADifferentValue(this.runChangeDetection);
	}

	private stopListening(): void {
		this.pache?.stopListeningForWhenRunningGetValueAgainMightReturnADifferentValue(this.runChangeDetection);
	}

	public transform<T>(pache: Pache<T>): T {
		if (this.pache != pache) {
			this.stopListening();
			this.pache = pache;
			this.startListening();
		}
		return pache.getValue();
	}

	public ngOnDestroy(): void {
		this.stopListening();
	}
}
