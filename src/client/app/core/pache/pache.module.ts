import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PachePipe} from './pache.pipe';

@NgModule({
	declarations: [PachePipe],
	exports: [PachePipe],
	imports: [CommonModule],
})
export class PachePipeModule {}
