import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {EntryModule} from './entry/entry.module';

@NgModule({
	declarations: [AppComponent],
	imports: [
		AppRoutingModule,
		BrowserModule,
		EntryModule,
		HttpClientModule, // In order to use `HttpClient`, we have to import `HttpClientModule` somewhere --> see restcaller.ts
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
