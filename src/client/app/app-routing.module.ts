import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EntryComponent} from './entry/entry.component';
import {RoomGuard} from './room.guard';

const routes: Routes = [
	{path: '', component: EntryComponent, pathMatch: 'full'},
	{
		path: `room/:${RoomGuard.roomCodeKey}`,
		canLoad: [RoomGuard],
		loadChildren: () => import('./room/room.module').then((m) => m.RoomModule),
	},
	{path: '**', redirectTo: '/'},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
