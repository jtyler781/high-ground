import {Component, ChangeDetectionStrategy, ChangeDetectorRef, NgZone} from '@angular/core';
import {ResponseReceiver} from '@client/controller/responsereceiver';
import {environment} from '@client/environments/environment';

@Component({
	selector: 'hg-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
	constructor(responseReceiver: ResponseReceiver) {
		responseReceiver.listen();
		console.log(environment.serverUrl);
	}
}
