import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {NYTCrosswordPuzzle} from '@client/games/crossword/model/nytcrossword/nytcrosswordpuzzle';
import {NYTCrosswordPuzzleClient} from '@client/games/crossword/presenter/nytcrosswordpuzzleclient';
import {CurrentPlayer} from '@client/presenter/currentplayer';
import {SerializedRoomJoinResponse} from '@shared/communication/roomjoin/roomjoinresponse';
import {RoomGuard} from '../room.guard';

@Injectable({providedIn: 'root'})
export class RoomResolver implements Resolve<unknown> {
	public serializedRoomJoinResponse?: SerializedRoomJoinResponse;
	public puzzle?: NYTCrosswordPuzzle;

	constructor(
		private readonly currentPlayer: CurrentPlayer,
		private readonly nytCrosswordPuzzleClient: NYTCrosswordPuzzleClient,
	) {}

	public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		const roomCode = route.paramMap.get(RoomGuard.roomCodeKey)?.toLowerCase() ?? '';
		this.serializedRoomJoinResponse = await this.currentPlayer.requestToJoinRoom(roomCode);
		this.puzzle = await this.nytCrosswordPuzzleClient.loadPuzzle(25, 12, 2001);
	}
}
