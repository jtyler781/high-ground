import {Component, ChangeDetectionStrategy, Input} from '@angular/core';
import {Router} from '@angular/router';
import {CrosswordBoxPresence} from '@client/games/crossword/presenter/crosswordboxpresence';
import {CurrentRoom, SerializedRoomJoinResponseInjectionTokenProvide} from './currentroom';

@Component({
	selector: 'hg-room',
	templateUrl: './room.component.html',
	styleUrls: ['./room.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [CurrentRoom, CrosswordBoxPresence, SerializedRoomJoinResponseInjectionTokenProvide],
})
export class RoomComponent {
	constructor(private readonly router: Router, public readonly room: CurrentRoom) {}

	public leaveRoom(): void {
		this.room.leaveRoom();
		this.router.navigate(['/']);
	}
}
