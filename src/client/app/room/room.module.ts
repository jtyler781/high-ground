import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RoomComponent} from './room.component';
import {HGInputModule} from 'client/core/input/input.module';
import {CrosswordGameModule} from '@client/games/crossword/view/crosswordgame/crosswordgame.module';
import {PachePipeModule} from '@client/app/core/pache/pache.module';
import {RoomRoutingModule} from './room-routing.module';

@NgModule({
	declarations: [RoomComponent],
	exports: [RoomComponent],
	imports: [CommonModule, HGInputModule, CrosswordGameModule, PachePipeModule, RoomRoutingModule],
})
export class RoomModule {}
