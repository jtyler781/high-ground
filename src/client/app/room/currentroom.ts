import {FactoryProvider, inject, Inject, Injectable, InjectionToken, OnDestroy} from '@angular/core';
import {ResponseReceiver} from '@client/controller/responsereceiver';
import {
	CrosswordBoxEntryUpdate,
	CrosswordBoxEntryUpdateFactory,
} from '@client/games/crossword/model/update/crosswordboxentryupdate';
import {CurrentPlayer} from '@client/presenter/currentplayer';
import {SerializedRoomJoinResponse} from '@shared/communication/roomjoin/roomjoinresponse';
import {RoomUpdate, SerializedRoomUpdateAction} from '@shared/communication/roomupdate';
import {SerializedUpdate} from '@shared/communication/update';
import {Player} from '@shared/model/player';
import {Room} from '@shared/model/room';
import {RoomResolver} from './room.resolver';

export const SerializedRoomJoinResponseInjectionToken = new InjectionToken<SerializedRoomJoinResponse>(
	'SerializedRoomJoinResponseInjectionToken',
);

export const SerializedRoomJoinResponseInjectionTokenProvide: FactoryProvider = {
	provide: SerializedRoomJoinResponseInjectionToken,
	useFactory: () => {
		const response = inject(RoomResolver).serializedRoomJoinResponse;
		if (!response) {
			throw new Error('Tried to inject current room but we have not loaded one');
		}
		return response;
	},
};

@Injectable()
export class CurrentRoom extends Room implements OnDestroy {
	public readonly serializedUpdates: readonly SerializedUpdate<unknown>[];

	constructor(
		@Inject(SerializedRoomJoinResponseInjectionToken) serializedRoomJoinResponse: SerializedRoomJoinResponse,
		roomResolver: RoomResolver,
		private readonly currentPlayer: CurrentPlayer,
		private readonly crosswordBoxEntryUpdateFactory: CrosswordBoxEntryUpdateFactory,
		private readonly responseReceiver: ResponseReceiver,
	) {
		super(serializedRoomJoinResponse['c']);
		const puzzle = roomResolver.puzzle;
		if (!puzzle) {
			// TODO Does this have to be loaded before constructing this class? Should we make an async/load-puzzle pache
			throw new Error('Tried to load current room but no crossword puzzle provided');
		}
		this.game.setValue(puzzle);

		for (const serializedPlayer of serializedRoomJoinResponse['p']) {
			const player = Player.createOtherPlayerInRoomFromSerializedPlayer(serializedPlayer);
			this.players.set(player.id, player);
		}
		this.serializedUpdates = serializedRoomJoinResponse['u'] ?? [];

		this.responseReceiver.addListener(RoomUpdate.id, this.updateRoom);
		this.responseReceiver.addListener(CrosswordBoxEntryUpdate.id, this.handleCrosswordBoxEntryUpdate);
		this.serializedUpdates.filter((update) => update['id'] == RoomUpdate.id).forEach(this.updateRoom);
		this.serializedUpdates
			.filter((update) => update['id'] == CrosswordBoxEntryUpdate.id)
			.forEach(this.handleCrosswordBoxEntryUpdate);
	}

	public ngOnDestroy(): void {
		this.responseReceiver.removeListener(RoomUpdate.id, this.updateRoom);
		this.responseReceiver.removeListener(CrosswordBoxEntryUpdate.id, this.handleCrosswordBoxEntryUpdate);
	}

	public leaveRoom(): void {
		this.currentPlayer.leaveRoom();
	}

	private readonly handleCrosswordBoxEntryUpdate = (serializedUpdate: SerializedUpdate<unknown>): void => {
		const update = this.crosswordBoxEntryUpdateFactory.deserialize(serializedUpdate);
		const puzzle = this.game.getValue();
		if (update && puzzle) {
			puzzle.handleCrosswordBoxEntryUpdate(update);
		}
	};

	private readonly updateRoom = (serializedUpdate: SerializedUpdate<unknown>): void => {
		const serializedRoomUpdateData = serializedUpdate['d'];
		if (!RoomUpdate.isSerializedRoomUpdateData(serializedRoomUpdateData)) {
			console.error('Received invalid room update push');
			return;
		}

		// TODO I think these should just be three different events
		if (serializedRoomUpdateData['a'] == SerializedRoomUpdateAction.Join) {
			const player = Player.createOtherPlayerInRoomFromSerializedPlayer(serializedRoomUpdateData['p']);
			this.players.set(player.id, player);
		} else if (serializedRoomUpdateData['a'] == SerializedRoomUpdateAction.Leave) {
			this.players.delete(serializedRoomUpdateData['p']['i']);
			// TODO clear presence for the user who just left? Or maybe send a "clear presence" update
		} else if (serializedRoomUpdateData['a'] == SerializedRoomUpdateAction.ChangeName) {
			this.currentPlayer.updatePlayerName(serializedRoomUpdateData['p']['n']);
		} else {
			console.error('Received room update event with unrecognized action: ' + serializedRoomUpdateData['a']);
		}
	};
}
