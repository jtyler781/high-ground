import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RoomComponent} from './room.component';
import {RoomResolver} from './room.resolver';

const routes: Routes = [
	{
		path: '',
		component: RoomComponent,
		resolve: {room: RoomResolver},
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class RoomRoutingModule {}
