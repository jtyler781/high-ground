import {Component, ChangeDetectionStrategy, Input} from '@angular/core';
import {Router} from '@angular/router';
import {RoomController} from '@client/controller/room.controller';
import {CurrentPlayer} from '@client/presenter/currentplayer';

@Component({
	selector: 'hg-entry',
	templateUrl: './entry.component.html',
	styleUrls: ['./entry.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntryComponent {
	@Input() public roomCode: string = '';

	public readonly name = this.currentPlayer.name;

	constructor(
		private readonly roomController: RoomController,
		private readonly currentPlayer: CurrentPlayer,
		private readonly router: Router,
	) {
		this.currentPlayer.leaveRoom(); // Leave a room if already in one when reaching this component
	}

	public join(): void {
		this.router.navigate(['/room', this.roomCode]);
	}

	public setPlayerName(name: string): void {
		this.roomController.updateName(name);
	}
}
