import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EntryComponent} from './entry.component';
import {HGInputModule} from 'client/core/input/input.module';
import {PachePipeModule} from '../core/pache/pache.module';

@NgModule({
	declarations: [EntryComponent],
	exports: [EntryComponent],
	imports: [CommonModule, HGInputModule, PachePipeModule],
})
export class EntryModule {}
