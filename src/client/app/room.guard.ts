import {Injectable} from '@angular/core';
import {UrlTree, Router, CanLoad, Route, UrlSegment} from '@angular/router';
import {CurrentPlayer} from '@client/presenter/currentplayer';
import {doesStringContainOnlyLetters} from '@client/util/regex';

@Injectable({providedIn: 'root'})
export class RoomGuard implements CanLoad {
	public static readonly roomCodeKey: string = 'code';

	constructor(private readonly router: Router, private readonly currentPlayer: CurrentPlayer) {}

	public canLoad(route: Route, segments: UrlSegment[]): boolean | UrlTree {
		const roomCode = segments[1].path;
		if (roomCode?.length != 4 || !doesStringContainOnlyLetters(roomCode)) {
			return this.router.parseUrl('/');
		}
		this.currentPlayer.requestToJoinRoom(roomCode);
		return true;
	}

	// TODO canActivate probably
}
