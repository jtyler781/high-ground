import {Injectable} from '@angular/core';
import {RequestSender} from '@client/controller/requestsender';
import {LocalStorage, LocalStorageKey} from '@client/util/localstorage';
import {generateUUID} from '@client/util/uuid';
import {RoomJoinRequest} from '@shared/communication/roomjoin/roomjoinrequest';
import {SerializedRoomJoinResponse} from '@shared/communication/roomjoin/roomjoinresponse';
import {RoomLeaveRequest} from '@shared/communication/roomleave/roomleaverequest';
import {Player} from '@shared/model/player';

@Injectable({providedIn: 'root'})
export class CurrentPlayer extends Player {
	public joinRoomPromise?: Promise<SerializedRoomJoinResponse>;
	public resolveJoinRoomPromise?: (value: SerializedRoomJoinResponse) => void;

	constructor(private readonly localStorage: LocalStorage, private readonly requestSender: RequestSender) {
		super(
			localStorage.get(LocalStorageKey.PLAYER_ID) ?? generateUUID(),
			localStorage.get(LocalStorageKey.PLAYER_NAME) ?? '',
		);
	}

	public updatePlayerName(name: string): void {
		if (this.name.getValue() != name) {
			this.name.setValue(name);
			this.localStorage.set(LocalStorageKey.PLAYER_NAME, name); // TODO make local-storage-backed pache
		}
	}

	public requestToJoinRoom(roomCode: string): Promise<SerializedRoomJoinResponse> {
		if (this.joinRoomPromise) {
			return this.joinRoomPromise;
		}
		this.requestSender.sendRequest(new RoomJoinRequest(this, roomCode));
		this.joinRoomPromise = new Promise<SerializedRoomJoinResponse>(
			(resolve, reject) => (this.resolveJoinRoomPromise = resolve),
		);
		return this.joinRoomPromise;
	}

	public receivedRoomJoinResponse(response: SerializedRoomJoinResponse): void {
		this.resolveJoinRoomPromise?.(response);
	}

	public leaveRoom(): void {
		if (this.joinRoomPromise || this.resolveJoinRoomPromise) {
			this.joinRoomPromise = undefined;
			this.resolveJoinRoomPromise = undefined;
			this.requestSender.sendRequest(new RoomLeaveRequest());
		}
	}
}
