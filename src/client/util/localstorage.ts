import {Injectable} from '@angular/core';

export enum LocalStorageKey {
	PLAYER_ID = 'PLAYER_ID',
	PLAYER_NAME = 'PLAYER_NAME',
}

@Injectable({providedIn: 'root'})
export class LocalStorage {
	public get(key: LocalStorageKey): string | undefined {
		return localStorage.getItem(key) ?? undefined;
	}

	public set(key: LocalStorageKey, value: string): void {
		localStorage.setItem(key, value);
	}
}
