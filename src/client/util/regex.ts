export function doesStringContainOnlyLetters(v: string): boolean {
	return /^[a-zA-Z]+$/.test(v);
}
