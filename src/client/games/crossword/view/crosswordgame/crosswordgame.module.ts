import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CrosswordGameComponent} from './crosswordgame.component';
import {CrosswordBoardModule} from '../crosswordboard/crosswordboard.module';
import {PachePipeModule} from '@client/app/core/pache/pache.module';

@NgModule({
	declarations: [CrosswordGameComponent],
	exports: [CrosswordGameComponent],
	imports: [CommonModule, CrosswordBoardModule, PachePipeModule],
})
export class CrosswordGameModule {}
