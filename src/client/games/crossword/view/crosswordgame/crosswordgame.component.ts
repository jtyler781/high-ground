import {Component, ChangeDetectionStrategy, Input} from '@angular/core';
import {CrosswordPuzzle} from '../../model/crosswordpuzzle';

@Component({
	selector: 'hg-crossword-game',
	templateUrl: './crosswordgame.component.html',
	styleUrls: ['./crosswordgame.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CrosswordGameComponent {
	@Input() public puzzle!: CrosswordPuzzle;
}
