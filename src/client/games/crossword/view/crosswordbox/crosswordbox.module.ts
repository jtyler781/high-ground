import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CrosswordBoxComponent} from './crosswordbox.component';
import {PachePipeModule} from '@client/app/core/pache/pache.module';
import {FormsModule} from '@angular/forms';

@NgModule({
	declarations: [CrosswordBoxComponent],
	exports: [CrosswordBoxComponent],
	imports: [CommonModule, FormsModule, PachePipeModule],
})
export class CrosswordBoxModule {}
