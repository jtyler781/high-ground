import {Component, ChangeDetectionStrategy, Input, HostBinding} from '@angular/core';
import {Player} from '@shared/model/player';
import {arrayEquals} from '@shared/pache/equality';
import {ReadonlyPache} from '@shared/pache/pache';
import {CrosswordBox} from '../../model/crosswordbox';
import {CrosswordBoxPresence} from '../../presenter/crosswordboxpresence';

@Component({
	selector: 'hg-crossword-box[box]',
	templateUrl: './crosswordbox.component.html',
	styleUrls: ['./crosswordbox.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CrosswordBoxComponent {
	@Input() public box!: CrosswordBox;

	@HostBinding('class.hg-crossword-box-entry') public isEntry: boolean = false;

	public readonly otherPlayersInThisBox = new ReadonlyPache<readonly Player[]>(
		() => this.crosswordBoxPresence.getOtherPlayersAtBox(this.box),
		arrayEquals,
	);

	constructor(private readonly crosswordBoxPresence: CrosswordBoxPresence) {}

	public ngOnInit(): void {
		this.isEntry = !!this.box.answer;
	}

	public setCaret(event: Event) {
		const inputElement = event.target;
		if (!(inputElement instanceof HTMLInputElement)) {
			return;
		}
		inputElement.setSelectionRange(0, inputElement.value.length);
	}

	public setEntry(letter: string): void {
		this.box.entry.setValue(letter || undefined);
	}

	public onFocus(): void {
		this.crosswordBoxPresence.focus(this.box.location);
	}
}
