import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CrosswordBoardComponent} from './crosswordboard.component';
import {CrosswordBoxModule} from '../crosswordbox/crosswordbox.module';

@NgModule({
	declarations: [CrosswordBoardComponent],
	exports: [CrosswordBoardComponent],
	imports: [CommonModule, CrosswordBoxModule],
})
export class CrosswordBoardModule {}
