import {Component, ChangeDetectionStrategy, Input} from '@angular/core';
import {CrosswordPuzzle} from '../../model/crosswordpuzzle';

@Component({
	selector: 'hg-crossword-board[puzzle]',
	templateUrl: './crosswordboard.component.html',
	styleUrls: ['./crosswordboard.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CrosswordBoardComponent {
	@Input() public puzzle!: CrosswordPuzzle;
}
