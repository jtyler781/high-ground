import {Injectable, OnDestroy} from '@angular/core';
import {CrosswordBoxLocation} from '@shared/games/crossword/crosswordboxlocation';
import {Player} from '@shared/model/player';
import {optEquals} from '@shared/pache/equality';
import {PacheMap} from '@shared/pache/pachemap';
import {CrosswordBox} from '../model/crosswordbox';
import {CrosswordBoxPresenceUpdate} from '../model/update/crosswordboxpresenceupdate';
import {CrosswordBoxPresencePache, CrosswordBoxPresencePacheFactory} from './crosswordboxpresencepache';
import {SerializedUpdate} from '@shared/communication/update';
import {CurrentRoom} from '@client/app/room/currentroom';
import {ResponseReceiver} from '@client/controller/responsereceiver';

@Injectable()
export class CrosswordBoxPresence implements OnDestroy {
	private readonly currentUserLocation: CrosswordBoxPresencePache;
	private readonly otherPlayerLocations = new PacheMap<Player, CrosswordBoxLocation | undefined>(
		optEquals(CrosswordBoxLocation.equals),
	);

	constructor(
		crosswordBoxPresencePacheFactory: CrosswordBoxPresencePacheFactory,
		private readonly currentRoom: CurrentRoom,
		private readonly responseReceiver: ResponseReceiver,
	) {
		this.currentUserLocation = crosswordBoxPresencePacheFactory.create(undefined);
		const serializedPresenceUpdates = currentRoom.serializedUpdates.filter(
			(serializedUpdate) => serializedUpdate['id'] == CrosswordBoxPresenceUpdate.id,
		);
		for (const serializedUpdate of serializedPresenceUpdates) {
			this.handleCrosswordBoxPresenceUpdate(serializedUpdate);
		}

		this.responseReceiver.addListener(CrosswordBoxPresenceUpdate.id, this.handleCrosswordBoxPresenceUpdate);
	}

	public ngOnDestroy(): void {
		this.responseReceiver.removeListener(CrosswordBoxPresenceUpdate.id, this.handleCrosswordBoxPresenceUpdate);
	}

	public focus(location: CrosswordBoxLocation): void {
		this.currentUserLocation.setValue(location);
	}

	public getOtherPlayersAtBox(box: CrosswordBox) {
		return Array.from(this.otherPlayerLocations.entries())
			.filter(([player, locationPache]) => locationPache.getValue()?.equals(box.location))
			.map(([player, locationPache]) => player);
	}

	public handleCrosswordBoxPresenceUpdate = (serializedUpdate: SerializedUpdate<unknown>): void => {
		const update = CrosswordBoxPresenceUpdate.deserialize(serializedUpdate);
		const player = this.currentRoom.players.values().find((player) => player.id == update.playerId);
		if (!player) {
			// When running these updates on loading a room, updates might be for players who have since left. Skip.
			return;
		}
		this.otherPlayerLocations.set(player, update.crosswordBoxLocation);
	};
}
