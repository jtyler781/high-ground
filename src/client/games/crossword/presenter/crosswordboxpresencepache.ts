import {Injectable} from '@angular/core';
import {CrosswordBoxLocation} from '@shared/games/crossword/crosswordboxlocation';
import {WriteablePache} from '@shared/pache/pache';
import {RequestSender} from '@client/controller/requestsender';
import {CrosswordBoxPresenceUpdate} from '../model/update/crosswordboxpresenceupdate';
import {CurrentPlayer} from '@client/presenter/currentplayer';

export class CrosswordBoxPresencePache extends WriteablePache<CrosswordBoxLocation | undefined> {
	constructor(
		private readonly requestSender: RequestSender,
		private readonly playerId: string,
		currentPresenceValue: CrosswordBoxLocation | undefined,
	) {
		super(() => currentPresenceValue);
	}

	public override setValue(value: CrosswordBoxLocation | undefined): void {
		super.setValue(value);
		this.requestSender.sendRequest(new CrosswordBoxPresenceUpdate(this.playerId, value));
	}

	public updateInternalValue(value: CrosswordBoxLocation | undefined): void {
		super.setValue(value);
	}
}

@Injectable({providedIn: 'root'})
export class CrosswordBoxPresencePacheFactory {
	constructor(private readonly currentPlayer: CurrentPlayer, private readonly requestSender: RequestSender) {}

	public create(initialPresenceValue: CrosswordBoxLocation | undefined): CrosswordBoxPresencePache {
		return new CrosswordBoxPresencePache(this.requestSender, this.currentPlayer.id, initialPresenceValue);
	}
}
