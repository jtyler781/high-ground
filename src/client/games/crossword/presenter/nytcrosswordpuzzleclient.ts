import {Injectable} from '@angular/core';
import {RestCaller} from '@shared/communication/restcaller';
import {NYTCrosswordPuzzle, NYTCrosswordPuzzleFactory} from '../model/nytcrossword/nytcrosswordpuzzle';
import {
	isSerializedNYTCrosswordPuzzle,
	SerializedNYTCrosswordPuzzle,
} from '../model/nytcrossword/serializednytcrosswordpuzzle';

@Injectable({providedIn: 'root'})
export class NYTCrosswordPuzzleClient {
	constructor(
		private readonly restCaller: RestCaller,
		private readonly nytCrosswordPuzzleFactory: NYTCrosswordPuzzleFactory,
	) {}

	public async loadPuzzle(day: number, month: number, year: number): Promise<NYTCrosswordPuzzle> {
		const url = this.getURL(day, month, year);
		const serializedPuzzle = await this.restCaller.get<SerializedNYTCrosswordPuzzle>(url);
		// TODO does the rest caller already type check?
		if (!isSerializedNYTCrosswordPuzzle(serializedPuzzle)) {
			console.error('TYPE GUARD RETURNED FALSE');
		}
		const puzzle = this.nytCrosswordPuzzleFactory.create(serializedPuzzle);
		return puzzle;
	}

	private getURL(day: number, month: number, year: number): string {
		return `https://raw.githubusercontent.com/doshea/nyt_crosswords/master/${year}/${month}/${day}.json`;
	}
}
