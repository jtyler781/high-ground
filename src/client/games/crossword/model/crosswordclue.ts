export interface CrosswordClue {
	readonly number: number;
	readonly clue: string;
}
