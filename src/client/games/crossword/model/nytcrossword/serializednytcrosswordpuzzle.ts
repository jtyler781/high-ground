import {isObject, isNumber, isString, isTypedArray} from '@shared/framework/typeguards';
import {isSerializedNYTCrosswordClues, SerializedNYTCrosswordPuzzleClues} from './serializednytcrosswordclue';

export interface SerializedNYTCrosswordPuzzle {
	readonly 'clues': SerializedNYTCrosswordPuzzleClues;
	readonly 'grid': readonly string[];
	readonly 'gridnums': readonly number[];
	readonly 'size': SerializedNYTCrosswordSize;
	readonly 'title': string;
	readonly 'copyright': string;
}

interface SerializedNYTCrosswordSize {
	readonly 'cols': number;
	readonly 'rows': number;
}

export const isSerializedNYTCrosswordPuzzle = (value: unknown): value is SerializedNYTCrosswordPuzzle => {
	return (
		isObject(value) &&
		'clues' in value &&
		'grid' in value &&
		'gridnums' in value &&
		'size' in value &&
		'title' in value &&
		'copyright' in value &&
		isSerializedNYTCrosswordClues(value['clues']) &&
		isTypedArray(value['grid'], isString) &&
		isTypedArray(value['gridnums'], isNumber) &&
		isSerializedNYTCrosswordSize(value['size']) &&
		value['grid'].length == value['size']['cols'] * value['size']['rows'] &&
		value['grid'].length == value['gridnums'].length &&
		isString(value['title']) &&
		isString(value['copyright'])
	);
};

const isSerializedNYTCrosswordSize = (value: unknown): value is SerializedNYTCrosswordSize => {
	return isObject(value) && 'cols' in value && 'rows' in value && isNumber(value['cols']) && isNumber(value['rows']);
};
