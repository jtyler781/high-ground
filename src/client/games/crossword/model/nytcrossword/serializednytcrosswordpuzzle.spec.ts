import {exampleJson} from './serializednytcrosswordpuzzle.test';
import {isSerializedNYTCrosswordPuzzle} from './serializednytcrosswordpuzzle';
import 'jasmine';

describe('serializednytcrosswordpuzzle', () => {
	it('should work', () => {
		expect(isSerializedNYTCrosswordPuzzle(exampleJson)).toBeTrue();
	});
});
