import {isObject, isString, isTypedArray} from '@shared/framework/typeguards';
import {CrosswordClue} from '../crosswordclue';
import {CrosswordPuzzleClues} from '../crosswordpuzzle';

export const deserializeNYTCrosswordPuzzleClues = (value: SerializedNYTCrosswordPuzzleClues): CrosswordPuzzleClues => {
	return {
		across: value['across'].map((serializedClue: string) => deserializeNYTCrosswordClue(serializedClue)),
		down: value['down'].map((serializedClue: string) => deserializeNYTCrosswordClue(serializedClue)),
	};
};

const NYTCrosswordClueRegex = /(^\d+)\. (.+$)/i;

const deserializeNYTCrosswordClue = (value: string): CrosswordClue => {
	const parsedClue: RegExpMatchArray | null = value.match(NYTCrosswordClueRegex);
	if (parsedClue == null) {
		return {number: 0, clue: 'nomerge'};
	}
	const number = +parsedClue[1];
	const clue = parsedClue[2];
	return {number, clue};
};

export interface SerializedNYTCrosswordPuzzleClues {
	readonly 'across': readonly string[];
	readonly 'down': readonly string[];
}

export const isSerializedNYTCrosswordClues = (value: unknown): value is SerializedNYTCrosswordPuzzleClues => {
	return (
		isObject(value) &&
		'across' in value &&
		'down' in value &&
		isTypedArray(value['across'], isString) &&
		isTypedArray(value['down'], isString)
	);
};
