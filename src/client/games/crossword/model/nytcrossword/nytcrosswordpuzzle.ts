import {Injectable} from '@angular/core';
import {CrosswordBoxLocation} from '@shared/games/crossword/crosswordboxlocation';
import {CrosswordBox, CrosswordBoxFactory} from '../crosswordbox';
import {CrosswordGrid, CrosswordPuzzle, CrosswordPuzzleClues, CrosswordPuzzleSize} from '../crosswordpuzzle';
import {deserializeNYTCrosswordPuzzleClues} from './serializednytcrosswordclue';
import {SerializedNYTCrosswordPuzzle} from './serializednytcrosswordpuzzle';

export class NYTCrosswordPuzzle extends CrosswordPuzzle {}

@Injectable({providedIn: 'root'})
export class NYTCrosswordPuzzleFactory {
	constructor(private readonly crosswordBoxFactory: CrosswordBoxFactory) {}

	public create(serializedPuzzle: SerializedNYTCrosswordPuzzle): NYTCrosswordPuzzle {
		const clues: CrosswordPuzzleClues = deserializeNYTCrosswordPuzzleClues(serializedPuzzle['clues']);
		const size: CrosswordPuzzleSize = {
			width: serializedPuzzle['size']['cols'],
			height: serializedPuzzle['size']['rows'],
		};
		const boxes: CrosswordBox[][] = [];
		for (let rowIndex = 0; rowIndex < size.width; rowIndex++) {
			boxes.push([]);
			for (let colIndex = 0; colIndex < size.height; colIndex++) {
				const serializedIndex = rowIndex * size.width + colIndex;
				const serializedNumber = serializedPuzzle['gridnums'][serializedIndex];
				const serializedLetter = serializedPuzzle['grid'][serializedIndex];
				const number = serializedNumber ? serializedNumber : undefined;
				const letter = serializedLetter != '.' ? serializedLetter : undefined;
				const location = new CrosswordBoxLocation(rowIndex, colIndex);
				const box = this.crosswordBoxFactory.create(location, number, letter);
				boxes[boxes.length - 1].push(box);
			}
		}
		return new NYTCrosswordPuzzle(serializedPuzzle['title'], boxes, clues, size, serializedPuzzle['copyright']);
	}
}
