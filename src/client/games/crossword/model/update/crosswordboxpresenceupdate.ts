import {SerializedUpdate, Update} from '@shared/communication/update';
import {MessageId} from '@shared/framework/message';
import {isString} from '@shared/framework/typeguards';
import {CrosswordBoxLocation} from '@shared/games/crossword/crosswordboxlocation';

export class CrosswordBoxPresenceUpdate extends Update {
	public static readonly id: MessageId = 'CrosswordBoxPresenceUpdate';

	constructor(
		public readonly playerId: string,
		public readonly crosswordBoxLocation: CrosswordBoxLocation | undefined,
	) {
		super(CrosswordBoxPresenceUpdate.id, playerId, crosswordBoxLocation?.serialize() ?? null);
	}

	public static deserialize(data: SerializedUpdate): CrosswordBoxPresenceUpdate {
		return new CrosswordBoxPresenceUpdate(
			data['p'],
			isString(data['d']) ? CrosswordBoxLocation.deserialize(data['d']) : undefined,
		);
	}
}

export class CrosswordBoxPresenceUpdatePush extends CrosswordBoxPresenceUpdate {
	constructor(public readonly recipientId: string, crosswordBoxPresenceUpdate: CrosswordBoxPresenceUpdate) {
		super(crosswordBoxPresenceUpdate.playerId, crosswordBoxPresenceUpdate.crosswordBoxLocation);
	}
}
