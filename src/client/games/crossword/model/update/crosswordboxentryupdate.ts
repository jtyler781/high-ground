import {CrosswordBoxLocation} from '@shared/games/crossword/crosswordboxlocation';
import {MessageId} from '@shared/framework/message';
import {Injectable} from '@angular/core';
import {CurrentPlayer} from '@client/presenter/currentplayer';
import {isObject, isOpt, isString} from '@shared/framework/typeguards';
import {SerializedUpdate, Update, UpdatePush} from '@shared/communication/update';

export class CrosswordBoxEntryUpdate extends Update {
	public static readonly id: MessageId = 'CrosswordBoxEntryUpdate';

	constructor(
		playerId: string,
		public readonly crosswordBoxLocation: CrosswordBoxLocation,
		public readonly letterEntry: string | undefined,
	) {
		super(CrosswordBoxEntryUpdate.id, playerId, {'e': letterEntry, 'l': crosswordBoxLocation.serialize()});
	}

	public static isSeserializedData(data: unknown): data is SerializedCrosswordBoxEntryUpdateData {
		return isObject(data) && isOpt(isString)(data['e']) && isString(data['l']);
	}
}

@Injectable({providedIn: 'root'})
export class CrosswordBoxEntryUpdateFactory {
	constructor(private readonly currentPlayer: CurrentPlayer) {}

	public create(
		crosswordBoxLocation: CrosswordBoxLocation,
		letterEntry: string | undefined,
	): CrosswordBoxEntryUpdate {
		return new CrosswordBoxEntryUpdate(this.currentPlayer.id, crosswordBoxLocation, letterEntry);
	}

	public deserialize(serializedUpdate: SerializedUpdate): CrosswordBoxEntryUpdate | undefined {
		const data = serializedUpdate['d'];
		if (!CrosswordBoxEntryUpdate.isSeserializedData(data)) {
			return undefined;
		}
		return new CrosswordBoxEntryUpdate(
			serializedUpdate['p'],
			CrosswordBoxLocation.deserialize(data['l']),
			data['e'] ?? undefined,
		);
	}
}

interface SerializedCrosswordBoxEntryUpdateData {
	readonly 'l': string; // Location
	readonly 'e': string | null;
}
