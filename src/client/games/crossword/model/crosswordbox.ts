import {Injectable} from '@angular/core';
import {CrosswordBoxEntry, CrosswordBoxEntryFactory} from '@client/controller/crosswordboxentry';
import {CrosswordBoxLocation} from '@shared/games/crossword/crosswordboxlocation';

export class CrosswordBox {
	public readonly entry: CrosswordBoxEntry;

	constructor(
		crosswordBoxEntryFactory: CrosswordBoxEntryFactory,
		public readonly location: CrosswordBoxLocation,
		public readonly number?: number,
		public readonly answer?: string,
		currentEntryValue?: string,
	) {
		this.entry = crosswordBoxEntryFactory.create(location, currentEntryValue);
	}
}

@Injectable({providedIn: 'root'})
export class CrosswordBoxFactory {
	constructor(private readonly crosswordBoxEntryFactory: CrosswordBoxEntryFactory) {}

	public create(
		location: CrosswordBoxLocation,
		number?: number,
		answer?: string,
		currentEntryValue?: string,
	): CrosswordBox {
		return new CrosswordBox(this.crosswordBoxEntryFactory, location, number, answer, currentEntryValue);
	}
}
