import {CrosswordBoxEntryUpdate} from './update/crosswordboxentryupdate';
import {CrosswordBox} from './crosswordbox';
import {CrosswordClue} from './crosswordclue';

export class CrosswordPuzzle {
	constructor(
		public readonly title: string,
		public readonly boxes: CrosswordGrid,
		public readonly clues: CrosswordPuzzleClues,
		public readonly size: CrosswordPuzzleSize,
		public readonly copyright: string,
	) {}

	public handleCrosswordBoxEntryUpdate(update: CrosswordBoxEntryUpdate): void {
		const box = this.boxes[update.crosswordBoxLocation.row][update.crosswordBoxLocation.column];
		box.entry.updateInternalValue(update.letterEntry);
	}

	public serialize(): SerializedCrosswordPuzzleState {
		const boxEntries = this.boxes.flatMap((row) =>
			row.map((box): SerializedCrosswordBoxEntry => [box.location.serialize(), box.entry.getValue()]),
		);
		const filledBoxEntries = boxEntries.filter(
			(serializedCrosswordBoxEntry): serializedCrosswordBoxEntry is SerializedFilledCrosswordBoxEntry =>
				!!serializedCrosswordBoxEntry[1],
		);
		return Object.fromEntries(filledBoxEntries);
	}
}

type SerializedCrosswordBoxEntry = [string, string | undefined];
type SerializedFilledCrosswordBoxEntry = [string, string];

export interface SerializedCrosswordPuzzleState {
	readonly [location: string]: string;
}

export type CrosswordGrid = readonly (readonly CrosswordBox[])[];

export interface CrosswordPuzzleClues {
	readonly across: CrosswordClue[];
	readonly down: CrosswordClue[];
}

export interface CrosswordPuzzleSize {
	readonly width: number;
	readonly height: number;
}
