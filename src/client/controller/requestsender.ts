import {Injectable} from '@angular/core';
import {Request} from '@shared/framework/message';
import {environment} from '../environments/environment';

const SERVER_URL = `ws://${environment.serverUrl}:8080`;

@Injectable({providedIn: 'root'})
export class RequestSender {
	private readonly socket = new WebSocket(SERVER_URL);
	private requestsToSendOnOpen: Request[] = [];
	public handleResponse: (messageEvent: MessageEvent) => void = () => {};

	public constructor() {
		this.socket.onmessage = (event: MessageEvent) => this.handleResponse(event);
		this.socket.onopen = () => this.sendRequestsOnOpen();
	}

	public sendRequest(request: Request): void {
		if (this.socket.readyState == this.socket.OPEN) {
			this.socket.send(JSON.stringify(request.serialize()));
		} else {
			this.requestsToSendOnOpen.push(request);
		}
	}

	private sendRequestsOnOpen(): void {
		for (const request of this.requestsToSendOnOpen) {
			this.socket.send(JSON.stringify(request.serialize()));
		}
		this.requestsToSendOnOpen = [];
	}
}
