import {Injectable} from '@angular/core';
import {RoomUpdate} from '@shared/communication/roomupdate';
import {CrosswordBoxEntryUpdate} from '@client/games/crossword/model/update/crosswordboxentryupdate';
import {RequestSender} from './requestsender';
import {CrosswordBoxPresenceUpdate} from '@client/games/crossword/model/update/crosswordboxpresenceupdate';
import {CurrentPlayer} from '@client/presenter/currentplayer';
import {SerializedUpdate, Update} from '@shared/communication/update';
import {isString} from '@shared/framework/typeguards';
import {RoomJoinResponse} from '@shared/communication/roomjoin/roomjoinresponse';
import {isSerializedMessage, MessageId} from '@shared/framework/message';
import {getWithDefaultSet} from '@shared/util/map';

@Injectable({providedIn: 'root'})
export class ResponseReceiver {
	private readonly listeners = new Map<MessageId, Set<(message: SerializedUpdate<unknown>) => void>>();

	public constructor(private readonly requestSender: RequestSender, private readonly currentPlayer: CurrentPlayer) {}

	public addListener(messageId: MessageId, cb: (message: SerializedUpdate<unknown>) => void): void {
		const listeners = getWithDefaultSet(this.listeners, messageId, () => new Set());
		listeners.add(cb);
	}

	public removeListener(id: string, cb: (serializedUpdate: SerializedUpdate<unknown>) => void) {
		this.listeners.get(id)?.delete(cb);
	}

	public listen(): void {
		this.requestSender.handleResponse = (event: MessageEvent<unknown>) => {
			const messageData = event.data;
			if (!isString(messageData)) {
				console.error('Received a message from the server whose data was not a string');
				return;
			}
			const messageJSON: unknown = JSON.parse(messageData);
			if (!isSerializedMessage(messageJSON)) {
				console.error('Received a message from the server but the message does not have an ID');
				return;
			}

			if (messageJSON['id'] == RoomJoinResponse.id) {
				if (!RoomJoinResponse.isSerializedRoomJoinResponse(messageJSON)) {
					console.error('Received invalid room join response');
					return;
				}
				this.currentPlayer.receivedRoomJoinResponse(messageJSON);
				return;
			}

			if (!Update.isSerializedUpdate(messageJSON)) {
				console.error('Trying to handle a serialized update that is not valid');
				return;
			}
			this.handleSerializedUpdate(messageJSON);
		};
	}

	public handleSerializedUpdate(serializedMessage: SerializedUpdate<unknown>): void {
		switch (serializedMessage['id']) {
			case RoomUpdate.id:
			case CrosswordBoxPresenceUpdate.id:
			case CrosswordBoxEntryUpdate.id:
				const listeners = this.listeners.get(serializedMessage['id']);
				if (!listeners) {
					console.error('no one is listening for ' + serializedMessage['id']);
					return;
				}
				listeners.forEach((cb) => cb(serializedMessage));
				return;
		}
		console.error({'Tried to parse an unrecognized message': serializedMessage});
	}
}
