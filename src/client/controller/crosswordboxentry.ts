import {Injectable} from '@angular/core';
import {CrosswordBoxLocation} from '@shared/games/crossword/crosswordboxlocation';
import {WriteablePache} from '@shared/pache/pache';
import {CrosswordBoxEntryUpdateFactory} from '@client/games/crossword/model/update/crosswordboxentryupdate';
import {RequestSender} from './requestsender';

export class CrosswordBoxEntry extends WriteablePache<string | undefined> {
	constructor(
		private readonly requestSender: RequestSender,
		private readonly crosswordBoxEntryUpdateFactory: CrosswordBoxEntryUpdateFactory,
		private readonly crosswordBoxLocation: CrosswordBoxLocation,
		currentEntryValue: string | undefined,
	) {
		super(() => currentEntryValue);
	}

	public override setValue(value: string | undefined): void {
		super.setValue(value);
		this.requestSender.sendRequest(this.crosswordBoxEntryUpdateFactory.create(this.crosswordBoxLocation, value));
	}

	public updateInternalValue(value: string | undefined): void {
		super.setValue(value);
	}
}

@Injectable({providedIn: 'root'})
export class CrosswordBoxEntryFactory {
	constructor(
		private readonly requestSender: RequestSender,
		private readonly crosswordBoxEntryUpdateFactory: CrosswordBoxEntryUpdateFactory,
	) {}

	public create(
		crosswordBoxLocation: CrosswordBoxLocation,
		currentEntryValue: string | undefined,
	): CrosswordBoxEntry {
		return new CrosswordBoxEntry(
			this.requestSender,
			this.crosswordBoxEntryUpdateFactory,
			crosswordBoxLocation,
			currentEntryValue,
		);
	}
}
