import {Injectable} from '@angular/core';
import {CurrentPlayer} from '@client/presenter/currentplayer';

// TODO probably get rid of this class?
@Injectable({providedIn: 'root'})
export class RoomController {
	public constructor(private readonly currentPlayer: CurrentPlayer) {}

	public updateName(name: string): void {
		this.currentPlayer.updatePlayerName(name);
		// TODO HG-4 if player in room, allow player to update name on server after joining room
	}
}
