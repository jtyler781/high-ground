import {Component, ChangeDetectionStrategy, Input, Output, EventEmitter} from '@angular/core';

@Component({
	selector: 'hg-input',
	templateUrl: './input.component.html',
	styleUrls: ['./input.component.less'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HGInputComponent {
	@Input() public value: string = '';
	@Input() public placeholder: string = '';
	@Output() public valueChange: EventEmitter<string> = new EventEmitter();

	constructor() {}

	public setValue(value: string): void {
		this.valueChange.emit(value);
	}
}
