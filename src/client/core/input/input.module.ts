import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {HGInputComponent} from './input.component';

@NgModule({
	declarations: [HGInputComponent],
	imports: [CommonModule, FormsModule],
	exports: [HGInputComponent],
})
export class HGInputModule {}
