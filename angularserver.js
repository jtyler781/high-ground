const express = require('express');

const _port = 3001;
const _app_folder = 'dist/client';

const app = express();

// ---- SERVE STATIC FILES ---- //
app.get('*.*', express.static(_app_folder, {maxAge: '1y'}));

// ---- SERVE APPLICATION PATHS ---- //
app.all('*', (req, res) => {
	res.status(200).sendFile(`/`, {root: _app_folder});
});

// ---- START UP THE NODE SERVER  ----
app.listen(_port, () => {
	console.log('Node Express server for ' + app.name + ' listening on http://localhost:' + _port);
});
